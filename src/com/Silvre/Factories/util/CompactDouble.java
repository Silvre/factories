package com.Silvre.Factories.util;

import java.text.DecimalFormat;

/**
 * Created by Dylan on 10/1/2016.
 *
 * Class: CompactDouble
 * Description: Class that represents a number written in exponential notation that allows for speed
 * in calculations, but sacrifices in accuracy.
 * Version: 1.0
 */
public class CompactDouble implements Comparable<CompactDouble> {
    private static int SIGNIFICANT_MAGNITUDE = 10;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00000");
    private static DecimalFormat storedFormat = new DecimalFormat("0.0000000000");
    private double value = 0;
    private int exponent = 0;

    /**
     * Default constructor with the value at 0 and exponent of 0.
     */
    public CompactDouble() {
    }

    /**
     * Constructor that accepts an integer as the value of the number. Truncated automatically.
     * @param i
     */
    public CompactDouble(int i) {
        value = i;
        truncate();
    }

    /**
     * Constructor that accepts the value of the number. Truncated automatically.
     * @param d
     */
    public CompactDouble(double d)  {
        value = d;
        truncate();
    }

    /**
     * Constructor that accepts the value and exponent of the number. Truncated automatically.
     * @param v
     * @param exp
     */
    public CompactDouble(double v, int exp)   {
        value = v;
        exponent = exp;
        truncate();
    }

    /**
     * Set the digit to the specified value. Truncated automatically.
     * @param d
     */
    public void setValue(double d)  {
        value = d;
        truncate();
    }

    /**
     * Set the exponent of the compact double.
     * @param e
     */
    public void setExponent(int e)  {
        exponent = e;
    }

    /**
     * Add two numbers, only does the operation if the numbers are within SIGNIFICANT_MAGNITUDE of each other,
     * or the adding number is far greater then the object this method is called from.
     * @param d
     */
    public CompactDouble add(CompactDouble d)    {
        if(this.getExponent() - d.getExponent() > SIGNIFICANT_MAGNITUDE)    {
            //Since this number is far greater(Orders of significant_magnitude magnitudes), the other number is insignificant.
        }
        else if(this.getExponent() - d.getExponent() < (SIGNIFICANT_MAGNITUDE * -1))  {
            this.exponent = d.getExponent();
            this.value = d.getValue();
            truncate();
        }
        else if(this.getExponent() == d.getExponent())  {
            value += d.getValue();
            truncate();
        }
        else    {
            //We now know that the two numbers we are adding are within a magnitude of each other by 5, so now we have to math.
            if(this.compareTo(d) == 1)  { //This number is larger than the other number
                int difference = this.getExponent() - d.getExponent();
                double d2 = d.getValue();
                for(int i = 0; i < difference; i++) {
                    d2 /= 10;
                }
                this.value += d2;
                truncate();
            }
            else if(this.compareTo(d) == -1) { //This number is smaller than the other number
                int difference = d.getExponent() - this.getExponent();
                for(int i = 0; i < difference; i++) {
                    value /= 10;
                }
                value += d.getValue();
                this.exponent += difference;
                truncate();
            }
        }
        return this;
    }

    /**
     * Subtract two numbers, only does the operation if the numbers are within SIGNIFICANT_MAGNITUDE of each other,
     * or the subtracting number is far greater then the object this method is called from.
     * @param d
     */
    public CompactDouble subtract(CompactDouble d)   {
        if(this.getExponent() - d.getExponent() > SIGNIFICANT_MAGNITUDE)    {
            //This number is too large and subtracting the second number would have no effect.
        }
        else if(this.getExponent() - d.getExponent() < (SIGNIFICANT_MAGNITUDE * -1))    {
            this.value = d.getValue() * -1;
            this.exponent = d.exponent;
            truncate();
        }
        else if(this.getExponent() == d.getExponent())  {
            this.value -= d.getValue();
            truncate();
        }
        else    {
            //We now know that the two numbers we are subtracting are within a magnitude of each other by 5, so now we have to math.
            if(this.compareTo(d) == 1)  { //This number is larger than the other number
                int difference = this.getExponent() - d.getExponent();  //This block brings the two values of the decimals
                double d2 = d.getValue();
                for(int i = 0; i < difference; i++) {
                    d2 /= 10;
                }
                this.value -= d2;
                truncate();
            }
            else if(this.compareTo(d) == -1) { //This number is smaller than the other number
                int difference = d.getExponent() - this.getExponent();
                for(int i = 0; i < difference; i++) {
                    value /= 10;
                }
                value -= d.getValue();
                this.exponent -= difference;
                truncate();
            }
        }
        return this;
    }

    /**
     * Mulitply two numbers, does not care about the proximity of the exponents.
     * @param d
     */
    public CompactDouble multiply(CompactDouble d)  {
        this.value *= d.getValue();
        this.exponent += d.getExponent();
        truncate();
        return this;
    }

    public CompactDouble multiply(int i) {
        this.value *= i;
        truncate();
        return this;
    }

    /**
     * Divide two numbers, does not care about the proximity of the exponents.
     * @param d
     */
    public CompactDouble divide(CompactDouble d)  {
        this.value /= d.getValue();
        this.exponent -= d.getExponent();
        truncate();
        return this;
    }

    //todo fix this.  this doesnt work as intended.
    public CompactDouble pow(double power)  {
        value *= power;
        exponent += power;
        truncate();
        return this;
    }

    //CompareTo method from Comparable Interface
    @Override
    public int compareTo(CompactDouble o) {
        if((getExponent() > o.getExponent()) || (getExponent() == o.getExponent() && getValue() > o.getValue())) {
            //System.out.println("Compare returned 1");
            return 1;
        }
        else if(getExponent() == o.getExponent() && getValue() == o.getValue())   {
            //System.out.println("Compare returned 0");
            return 0;
        }
        else if((getExponent() < o.getExponent()) || (getExponent() == o.getExponent() && getValue() < o.getValue())) {
            //System.out.println("Compare returned -1");
            return -1;
        }
        else    return -2;
    }

    /**
     * Return the exponent of the CompactDouble
     * @return
     */
    public int getExponent()    {   return exponent;    }

    /**
     * Return the value of the CompactDouble
     * @return
     */
    public double getValue()    {   return value;   }

    private void truncate() {
        //System.out.println("Number being truncated is " + this);
        while(value >= 10 || value <= -10)   {
            value /= 10;
            exponent++;
        }
        while((value < 1 && value > 0) || (value >  -1 && value < 0))  {
            value *= 10;
            exponent--;
        }
        if(value == 0.0) exponent = 0;
        value = Double.valueOf(storedFormat.format(value));
    }

    /**
     *
     * @param d CompactDouble to compare to.
     * @return
     */
    public boolean withinMagnitude(CompactDouble d)  {
        if(this.getExponent() - d.getExponent() > SIGNIFICANT_MAGNITUDE || this.getExponent() - d.getExponent() < SIGNIFICANT_MAGNITUDE * -1)   {
            return false;
        }
        return true;
    }

    /**
     * toString representation of the compactDouble.  Written in exponential notation.
     * Also rounds.
     * @return
     */
    public String toString()    {
        DecimalFormat form;
        switch(exponent)    {
            //TODO add case 6,7,8,9, and 10.
            case 0:
                form = new DecimalFormat("0");
                value = Double.parseDouble(form.format(value));
                break;
            case 1:
            case -1:
                form = new DecimalFormat("0.0");
                value = Double.parseDouble(form.format(value));
                break;
            case 2:
            case -2:
                form = new DecimalFormat("0.00");
                value = Double.parseDouble(form.format(value));
                break;
            case 3:
            case -3:
                form = new DecimalFormat("0.000");
                value = Double.parseDouble(form.format(value));
                break;
            case 4:
            case -4:
                form = new DecimalFormat("0.0000");
                value = Double.parseDouble(form.format(value));
                break;
            case 5:
            case -5:
                form = new DecimalFormat("0.00000");
                value = Double.parseDouble(form.format(value));
                break;
            default:
        }
        return decimalFormat.format(value) + "e" + exponent;
    }

    public CompactDouble clone()    {
        return new CompactDouble(getValue(), getExponent());
    }

    public CompactDouble get()  {
        return this;
    }

    public String toStrings()   {   return decimalFormat.format(value) + "e" + exponent; }

    /**
     * toString representation of the compactDouble, but with more decimal. Written in exponential notation.
     * @return
     */
    public String toDebugString()   {   return value + "e" + exponent; }
}