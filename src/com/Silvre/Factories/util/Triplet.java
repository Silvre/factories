package com.Silvre.Factories.util;

/**
 * Created by Dylan(Silvre) on 10/10/2016.
 *
 * Project: Factories
 */
public class Triplet<F, S, T> {
    private F f;
    private S s;
    private T t;

    public Triplet(F f, S s, T t)   {
        this.f = f;
        this.s = s;
        this.t = t;
    }

    public F getF() {   return f;   }
    public S getS() {   return s;   }
    public T getT() {   return t;   }

    public boolean equals(Triplet e)  {
        return (e.getF().equals(getF()) && e.getS().equals(getS()) && e.getT().equals(getT()));
    }

    public void setF(F f)   {   this.f = f; }
    public void setS(S s)   {   this.s = s; }
    public void setT(T t)   {   this.t = t; }
}
