package com.Silvre.Factories.util;

/**
 * Created by Dylan(Silvre) on 10/17/2016.
 * <p>
 * Class: ${CLASS}
 */
public class Duplet<F, S> {
    private F f;
    private S s;
    public Duplet(F f, S s) {
        this.f = f;
        this.s = s;
    }

    public F getF() {   return f;   }
    public S getS() {   return s;   }
}
