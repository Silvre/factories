package com.Silvre.Factories.game;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.ResourceBundle;

public class ResourceController implements Initializable    {
	
	@FXML
	private ListView list;
    private ObservableList<String> obs = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb)  {
        list.setItems(ResourceManager.resources);
    }

}
