package com.Silvre.Factories.game;

/**
 * Created by Dylan(Silvre) on 9/29/2016.
 *
 * Project: Factories
 */
public class EverySecond {
    public static void run() {
        addItems();
    }

    public static void addItems()  {
        ResourceManager.tickPrimaryFactories();
        ResourceManager.tickSecondaryFactories();
        ResourceManager.tickSellers();
        if(Core.MainWindow != null) Core.MainWindow.updateMoney();
        if(Core.ShopControl != null)    Core.ShopControl.updateMoney();
    }
}
