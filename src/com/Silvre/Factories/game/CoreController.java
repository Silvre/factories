package com.Silvre.Factories.game;

import com.Silvre.Factories.io.AES;
import com.Silvre.Factories.io.PlayerInfo;
import com.Silvre.Factories.util.CompactDouble;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CoreController implements Initializable    {

    protected static ResourceController resourceWindow;
    private static int timesClicked = 0;
	
	@FXML
	private Button resourcesButton;
    @FXML private Label gemDisplay;
    @FXML private Button shopButton;
    @FXML private Button getMoneyButton;
    @FXML private ScrollPane resourcePane;
    @FXML private Button saveButton;


	public void goToShop(ActionEvent e) throws Exception  {
        Core.toShopMenu();
    }

    public void tutorialMoney(ActionEvent e)    {
        ResourceManager.addMoney(new CompactDouble(1, 0));
        Core.stats.addTutorialClicks(1);
        if(Core.stats.getTutorialClicks() >= 100) {
            if(Constants.DEBUG) System.out.println("The player has clicked 100 times, disabling the button. \nBetter hope they didnt make some bad purchases");
            getMoneyButton.setVisible(false);
            Core.stats.setIsFirstTime(false);
        }
        /*Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Dank Memes Ensue");
        a.setHeaderText(null);
        a.setContentText("This is purely a testing and demonstrational alert.\n" +
                "If you received this, then it worked!!!!1");
        a.showAndWait();*/
    }

    public void save(ActionEvent e) {
        AES.encrypt(Core.gson.toJson(new PlayerInfo(Core.stats)), "superdankkeyamiritenicememe123");
    }

    public void updateMoney()   {
        gemDisplay.setText("Money: " + ResourceManager.getMoney());
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!Core.stats.isFirstTime())   {
            this.getMoneyButton.setVisible(false);
        }
        this.updateMoney();
        try {
            Node n = FXMLLoader.load(Constants.resourceMenu);
            resourcePane.setContent(n);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadSave(ActionEvent event) throws IOException {
	    Core.loadGame();
    }
}
