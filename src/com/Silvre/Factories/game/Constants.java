package com.Silvre.Factories.game;

import javafx.scene.image.Image;

import java.net.URL;

public final class Constants {
	public static final String GAME_NAME = "Factory Incremental";
	public static final String ASSETS_PATH = "/com/Silvre/Factories/assets/";
	public static final String FXML_PATH = "/com/Silvre/Factories/fxml/";
	public static final String GAME_PATH = "/com/Silvre/Factories/game/";
	public static final boolean DEBUG = true;

    public static final URL mainMenu = Constants.class.getResource("/com/Silvre/Factories/fxml/Core.fxml");
    public static final URL resourceMenu = Constants.class.getResource("/com/Silvre/Factories/fxml/Resource.fxml");
    public static final URL shopMenu = Constants.class.getResource("/com/Silvre/Factories/fxml/Shop.fxml");
    public static final Image icon = new Image("/com/Silvre/Factories/assets/IconJ.png");
    public static final URL loadMenu = Constants.class.getResource("/com/Silvre/Factories/fxml/LoadSaveData.fxml");
}
