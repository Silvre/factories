package com.Silvre.Factories.game;


import java.util.Scanner;

/**
 * Created by Dylan(Silvre) on 10/1/2016.
 */
public class Test {

//    static CompactDouble d = new CompactDouble(10, 0);
//
//    public static void main(String[] args) {
//        CompactDouble d = new CompactDouble();
//        CompactDouble max = new CompactDouble(1, 8);
//        CompactDouble one = new CompactDouble(1);
//        long time = System.currentTimeMillis();
//        while(d.compareTo(max) != 0)    {
//            d.add(one);
//        }
//        System.out.println("That operation took " + (System.currentTimeMillis() - time) + "ms");
//        int i = 0;
//        time = System.currentTimeMillis();
//        while(i != 1000000000) i++;
//        System.out.println("That operation took " + (System.currentTimeMillis() - time) + "ms");
//        BigInteger bb = new BigInteger("0");
//        BigInteger bmax = new BigInteger("100000000");
//        BigInteger bone = new BigInteger("1");
//        time = System.currentTimeMillis();
//        while(!bb.equals(bmax)) bb = bb.add(bone);
//        System.out.println("That operation took " + (System.currentTimeMillis() - time) + "ms");
//
//    }

//    public static void main(String[] args) throws AWTException {
//        Robot r = new Robot();
//    }

//    public static void main(String[] args) {
//        ArrayList<Special> array = new ArrayList<>();
//        array.add(new Special(1));
//        array.add(new Special(2));
//        array.add(new Special(0));
//        array.add(new Special(100));
//        array.add(new Special(50));
//        System.out.println(array);
//        Collections.sort(array);
//        System.out.println(array);
//    }

    public static class Special implements Comparable<Special>    {
        int number;
        public Special(int num) {
            number = num;
        }

        @Override
        public int compareTo(Special o) {
            return Integer.compare(number, o.number);
        }

        public String toString()    {
            return "" + number;
        }
    }

    public static void main(String[] args)  {
        Scanner s = new Scanner(System.in);
        int sem = 320;
        int e = 0;
        System.out.println("Are you exempting finals?");
        String exempt = s.next();
        if(exempt.equals("Y")) {
            //executes 2 lines, needs brackets
            System.out.println("What did you get on your final?");
            e = s.nextInt();
        }
        if(exempt.equals("Y")) //only one line, no need for bracket
            System.out.println((sem + e) / 7);
        else
            System.out.println(sem / 6);
    }
}
