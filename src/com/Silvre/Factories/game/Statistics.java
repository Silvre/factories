package com.Silvre.Factories.game;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.io.PlayerInfo;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;
import com.Silvre.Factories.util.Duplet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Dylan(Silvre) on 10/16/2016.
 *
 * Class: Statistics
 */
public class Statistics {
    private boolean isFirstTime = true;
    private int tutorialClicks = 0;
    private BigDecimal val = new BigDecimal("82571575732853615723169587265356173825632785623156325.129387132895206671269378467234961283432");
    private CompactDouble money = new CompactDouble(5, 10);
    public Set<Factory> unlockedPrimaryFactories = new TreeSet<>();
    public Set<Factory> unlockedSellerFactories = new TreeSet<>();
    public Set<Factory> unlockedSecondaryFactories = new TreeSet<>();
    public static ArrayList<Duplet<Resource, CompactDouble>> resourceList = new ArrayList<>();
    public ObservableList<Object> unlockedT1FactoriesList = FXCollections.observableArrayList();
    public ObservableList<Object> unlockedSellerFactoriesList = FXCollections.observableArrayList();
    public ObservableList<Object> unlockedSecondaryFactoriesList = FXCollections.observableArrayList();

    public Statistics(PlayerInfo p) {
        //Initialize data from playerinfo object, from loading and saving info.
        isFirstTime = p.isFirstTime;
        tutorialClicks = p.tutorialClicks;
        p.unlockedPrimary.forEach(x -> unlockedPrimaryFactories.add(getFactoryByName(x)));
        p.unlockedSeller.forEach(x-> unlockedSecondaryFactories.add(getFactoryByName(x)));
        p.unlockedSecondary.forEach(x-> unlockedSecondaryFactories.add(getFactoryByName(x)));
        updatePurchasableFactoriesList();
    }

    public Statistics() {

    }

    public static void initializeList() {
        for(Resource r : Resource.values()) {
            if(r.getID() != -1) {
                resourceList.add(new Duplet<>(r, new CompactDouble(0, 0)));
                ResourceManager.resources.add("???");
            }
        }
    }

    /*
        A simple note, if you want to add money not via a factory implementation, just use getMoney()) and add to that.
        If you plan to make a seller -esque factory, use a product of MONEY and it will be automatically added.
     */
    public void add(Resource e, CompactDouble d)    {
        if(e.getID() == -1) {
            addMoney(d);
            return;
        }
        for(int i = 0; i < resourceList.size(); i++)    {
            Duplet<Resource, CompactDouble> duplet = resourceList.get(i);
            if(duplet.getF() == e) {
                duplet.getS().add(d);
                ResourceManager.resources.set(i, duplet.getF().getName() + ": " + duplet.getS());
            }
        }
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    public void updatePurchasableFactoriesList()    {
        unlockedT1FactoriesList.clear();
        unlockedT1FactoriesList.add("Select an Item");
        unlockedSellerFactoriesList.clear();
        unlockedSellerFactoriesList.add("Select an Item");
        unlockedSecondaryFactoriesList.clear();
        unlockedSecondaryFactoriesList.add("Select an Item");
        for(Factory t : unlockedPrimaryFactories) {
            if(!unlockedT1FactoriesList.contains(t.toString())) unlockedT1FactoriesList.add(t.toString());
        }
        for(Factory t : unlockedSellerFactories)    {
            if(!unlockedSellerFactoriesList.contains(t.toString())) unlockedSellerFactoriesList.add(t.toString());
        }
        for(Factory t : unlockedSecondaryFactories) {
            if(!unlockedSecondaryFactoriesList.contains(t.toString())) unlockedSecondaryFactoriesList.add(t.toString());
        }
    }

    public int getTutorialClicks()  {
        return tutorialClicks;
    }

    public void addTutorialClicks(int delta)    {
        tutorialClicks += delta;
    }

    public void setIsFirstTime(boolean b)    {
        isFirstTime = b;
    }

    public CompactDouble getMoney() {
        return money;
    }

    public void setMoney(CompactDouble money) {
        this.money = money;
    }

    public void addMoney(CompactDouble money)   {
        this.money.add(money);
    }

    public void subtractMoney(CompactDouble money)  {
        this.money.subtract(money);
    }

    public static Factory getFactoryByName(String s)   {
        for(Factory f : ResourceManager.cleanFactories)  if(f.getName().equals(s)) return f.clone();
        return null;
    }

    public static String getFactoryName(Factory f) {
        return f.getName();
    }
}
