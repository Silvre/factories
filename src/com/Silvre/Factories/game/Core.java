package com.Silvre.Factories.game;

import com.Silvre.Factories.factories.*;
import com.google.gson.Gson;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class Core extends Application	{

    public static CoreController MainWindow;
    public static ShopController ShopControl;
    public static Stage mainStage;
    public static Statistics stats = new Statistics();
    public static Stage loadGameStage;
    public static LoadDataController loadDataController;
    public static Gson gson = new Gson();
	
	public static void main(String[] args)	{
		launch(args);
	}

	@Override
	public void start(Stage s) throws Exception {
        mainStage = s;
        //WoooOOOOooo LoADinG uP
        //LOAD UP THEM TIMERS BOI
        initializeTimers();

        //Register those Primary Factories
        ResourceManager.registerFactoryManager(new PrimaryWoodLoggerManager());
        ResourceManager.registerFactoryManager(new PrimaryStoneQuarry());
        ResourceManager.registerFactoryManager(new PrimaryCoalMine());
        ResourceManager.registerFactoryManager(new PrimaryWaterPump());

        //Register those Sellers
        ResourceManager.registerSeller(new WoodSeller());
        ResourceManager.registerSeller(new StoneSeller());
        ResourceManager.registerSeller(new CoalSeller());

        //Register those Secondary Factories
        ResourceManager.registerSecondaryFactory(new SecondaryWaterRefinery());
        ResourceManager.registerSecondaryFactory(new SecondaryCharcoalFactory());

        FXMLLoader loader = new FXMLLoader(Constants.mainMenu);
		Parent p = loader.load();
        MainWindow = loader.getController();
		Scene scene = new Scene(p, 1200, 675);
		s.getIcons().add(Constants.icon);
		s.setScene(scene);
        scene.getStylesheets().add("/com/Silvre/Factories/css/scrollbar.css");
		s.setResizable(false);
		s.setTitle(Constants.GAME_NAME);
		stats.initializeList();
		s.show();
	}
	
	private void initializeTimers() {
        Timeline t = new Timeline(new KeyFrame(Duration.seconds(1), event -> EverySecond.run()));
        t.setCycleCount(Animation.INDEFINITE);
        t.play();
    }

    /**
     * Set the current menu on the main window to the shop menu
     * @throws IOException
     */
    public static void toShopMenu() throws IOException {
        stats.updatePurchasableFactoriesList();
        FXMLLoader loader = new FXMLLoader(Constants.shopMenu);
        Parent p = loader.load();
        ShopControl = loader.getController();
        Scene scene = new Scene(p, 1200, 675);
        mainStage.setScene(scene);

    }

    /**
     * Set the current menu on the main window to the main menu
     * @throws IOException
     */
    public static void toMainMenu() throws IOException {
        FXMLLoader loader = new FXMLLoader(Constants.mainMenu);
        Parent p = loader.load();
        MainWindow = loader.getController();
        Scene scene = new Scene(p, 1200, 675);
        mainStage.setScene(scene);
    }

    public static void loadGame() throws IOException {
        loadGameStage = new Stage();
        FXMLLoader loader = new FXMLLoader(Constants.loadMenu);
        Parent p = loader.load();
        loadDataController = loader.getController();
        Scene s = new Scene(p, 600, 200);
        loadGameStage.setScene(s);
        loadGameStage.getIcons().addAll(Constants.icon);
        loadGameStage.setResizable(false);
        loadGameStage.setTitle(Constants.GAME_NAME);
        loadGameStage.show();
    }

}
