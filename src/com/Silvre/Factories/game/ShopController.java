package com.Silvre.Factories.game;

import com.Silvre.Factories.handler.PrimaryFactoryItemSelectedHandler;
import com.Silvre.Factories.handler.SecondaryFactoryItemSelectedHandler;
import com.Silvre.Factories.handler.SellerItemSelectedHandler;
import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Dylan(Silvre) on 10/14/2016.
 *
 * Project: Factories
 */
public class ShopController implements Initializable {
    @FXML private Label gemDisplay;
    @FXML private Button backToMainButton;

    @FXML private ChoiceBox buyPrimaryItemsBox;
    @FXML public Button buyPrimaryFactoryButton;
    @FXML public Label primaryDesc;

    @FXML private ChoiceBox sellerItemsBox;
    @FXML public Button buySellerButton;
    @FXML public Label sellerDesc;

    @FXML private ChoiceBox buySecondaryItemsBox;
    @FXML public Button buySecondaryButton;
    @FXML public Label secondaryDesc;

    public Factory selectedPrimaryFactory;
    public Factory selectedSellerFactory;
    public Factory selectedSecondaryFactory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateMoney();
        buyPrimaryItemsBox.setItems(Core.stats.unlockedT1FactoriesList);
        buyPrimaryItemsBox.getSelectionModel().selectedItemProperty().addListener(new PrimaryFactoryItemSelectedHandler());
        sellerItemsBox.setItems(Core.stats.unlockedSellerFactoriesList);
        sellerItemsBox.getSelectionModel().selectedItemProperty().addListener(new SellerItemSelectedHandler());
        buySecondaryItemsBox.setItems(Core.stats.unlockedSecondaryFactoriesList);
        buySecondaryItemsBox.getSelectionModel().selectedItemProperty().addListener(new SecondaryFactoryItemSelectedHandler());
        //Spawn in items and shit.
    }

    public void backToMain(ActionEvent e) throws IOException {
        Core.toMainMenu();
    }

    public void updateMoney()   {
        gemDisplay.setText("" + ResourceManager.getMoney());
    }

    public void buyPrimaryFactory(ActionEvent e)    {
        if(selectedPrimaryFactory != null) selectedPrimaryFactory.purchaseItem();
        updatePrimaryButton();
        updateMoney();
        updatePrimaryDescription();
    }

    public void buySeller(ActionEvent e)    {
        if(selectedSellerFactory != null)   selectedSellerFactory.purchaseItem();
        updateSellerButton();
        updateMoney();
        updateSellerDescription();
    }

    public void buySecondaryFactory(ActionEvent e)  {
        if(selectedSecondaryFactory != null) selectedSecondaryFactory.purchaseItem();
        updateSecondaryButton();
        updateMoney();
        updateSecondaryDescription();
    }

    public void updatePrimaryButton()   {
        if(selectedPrimaryFactory == null)   buyPrimaryFactoryButton.setDisable(true);
        else if(selectedPrimaryFactory.getPrice().compareTo(ResourceManager.getMoney()) > 0)    buyPrimaryFactoryButton.setDisable(true);
        else    buyPrimaryFactoryButton.setDisable(false);
    }

    public void updateSellerButton()    {
        if(selectedSellerFactory == null)   buySellerButton.setDisable(true);
        else if(selectedSellerFactory.getPrice().compareTo(ResourceManager.getMoney()) > 0)    {
            buySellerButton.setDisable(true);
        }
        else    buySellerButton.setDisable(false);
    }

    public void updateSecondaryButton() {
        if(selectedSecondaryFactory == null)    buySecondaryButton.setDisable(true);
        else if(selectedSecondaryFactory.getPrice().compareTo(ResourceManager.getMoney()) > 0)  {
            buySecondaryButton.setDisable(true);
        }
        else buySecondaryButton.setDisable(false);
    }

    public void updatePrimaryDescription()  {
        if(selectedPrimaryFactory != null) {
            String text = selectedPrimaryFactory.getName() + "\n------------------------------\nOwned: " + selectedPrimaryFactory.getNumberOwned()
                    + "\n\nPrice: " + selectedPrimaryFactory.getPrice() + " \n\nProducts:\n";
            for(Resource r : selectedPrimaryFactory.getProducts().keySet()) {
                text += "\t" + r.getName() + ": " + selectedPrimaryFactory.getProducts().get(r) + "\n";
            }
            primaryDesc.setText(text);
        }
        else primaryDesc.setText("");
    }

    public void updateSellerDescription()  {
        if(selectedSellerFactory != null) {
            String text = selectedSellerFactory.getName() + "\n------------------------------\nOwned: " + selectedSellerFactory.getNumberOwned()
                    + "\n\nPrice: " + selectedSellerFactory.getPrice() + " \n\nReactants:\n";
            for(Resource r : selectedSellerFactory.getIngredients().keySet())
                text += "\t" + r.getName() + ": " + selectedSellerFactory.getIngredients().get(r) + "\n";
            text += "Products:\n";
            for(Resource r : selectedSellerFactory.getProducts().keySet()) {
                text += "\t" + r.getName() + ": " + selectedSellerFactory.getProducts().get(r) + "\n";
            }
            sellerDesc.setText(text);
        }
        else sellerDesc.setText("");
    }

    public void updateSecondaryDescription()  {
        if(selectedSecondaryFactory != null) {
            String text =selectedSecondaryFactory.getName() + "\n------------------------------\nOwned: " +selectedSecondaryFactory.getNumberOwned()
                    + "\n\nPrice: " +selectedSecondaryFactory.getPrice() + " \n\nReactants:\n";
            for(Resource r :selectedSecondaryFactory.getIngredients().keySet())
                text += "\t" + r.getName() + ": " +selectedSecondaryFactory.getIngredients().get(r) + "\n";
            text += "Products:\n";
            for(Resource r :selectedSecondaryFactory.getProducts().keySet()) {
                text += "\t" + r.getName() + ": " +selectedSecondaryFactory.getProducts().get(r) + "\n";
            }
            secondaryDesc.setText(text);
        }
        else secondaryDesc.setText("");
    }

}