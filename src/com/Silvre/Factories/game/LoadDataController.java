package com.Silvre.Factories.game;

import com.Silvre.Factories.io.AES;
import com.Silvre.Factories.io.PlayerInfo;
import com.google.gson.JsonSyntaxException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

import static com.Silvre.Factories.game.Core.gson;

/**
 * Created by Team Tower Defense on 4/10/2017.
 */
public class LoadDataController implements Initializable {
    @FXML private Button loadButton;
    @FXML private TextArea outputBox;
    @FXML private Label errorText;

    public void load(ActionEvent a) {
        String data = outputBox.getText().trim();
        System.out.println(data);
        try {
            PlayerInfo p = gson.fromJson(AES.decrypt(data, "superdankkeyamiritenicememe123"), PlayerInfo.class);
            Core.stats = new Statistics(p);
            Core.stats.updatePurchasableFactoriesList();
            Core.MainWindow.updateMoney();
            if(Core.ShopControl != null)    Core.ShopControl.updateMoney();
        }
        catch (JsonSyntaxException e)   {
            errorText.setText("Bad save data!");
            System.out.println("Bad java syntax");
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }
}
