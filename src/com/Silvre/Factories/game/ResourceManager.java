package com.Silvre.Factories.game;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;
import com.Silvre.Factories.util.Duplet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import static com.Silvre.Factories.game.Core.stats;

/**
 * Created by Dylan(Silvre) on 9/28/2016.
 *
 * Project: Factories
 */
public class ResourceManager {
    public static Set<Factory> PrimaryFactories = new TreeSet<>();
    public static Set<Factory> Sellers = new TreeSet<>();
    public static Set<Factory> SecondaryFactories = new TreeSet<>();
    public static Set<Factory> Factories = new TreeSet<>();
    public static Set<Factory> cleanFactories = new TreeSet<>();
    public static ObservableList<String> resources = FXCollections.observableArrayList();

    public static void subtract(Resource r, CompactDouble d)    {
        for (int i = 0; i < stats.resourceList.size(); i++) {
            Duplet<Resource, CompactDouble> duplet = stats.resourceList.get(i);
            if(duplet.getF() == r)  {
                duplet.getS().subtract(d);
                resources.set(i, duplet.getF().getName() + ": " + duplet.getS());
            }
        }
    }

    public static CompactDouble getAmount(Resource r)  {
        for(Duplet<Resource, CompactDouble> d : stats.resourceList)   {
            if(d.getF() == r) return d.getS();
        }
        return null;
    }

    public static CompactDouble getMoney()   { return Core.stats.getMoney(); }
    public static void addMoney(CompactDouble delta)  {
        Core.stats.getMoney().add(delta);
        for(Factory t : PrimaryFactories) {
            if(!stats.unlockedPrimaryFactories.contains(t) && t.getConstructionType() != Factory.ConstructionType.BUILDABLE)    {
                if(getMoney().compareTo(t.getPrice()) >= 0)  {
                    stats.unlockedPrimaryFactories.add(t);
                    stats.updatePurchasableFactoriesList();
                    if(Constants.DEBUG) System.out.println("Added primary factory to the list: " + t);
                }
            }
        }
        for(Factory f : SecondaryFactories) {
            if(!stats.unlockedSecondaryFactories.contains(f) && f.getConstructionType() != Factory.ConstructionType.BUILDABLE)  {
                if(getMoney().compareTo(f.getPrice()) >= 0)  {
                    stats.unlockedSecondaryFactories.add(f);
                    stats.updatePurchasableFactoriesList();;
                    if(Constants.DEBUG) System.out.println("Added a secondary seller to the list: " + f);
                }
            }
        }
        for(Factory t : Sellers)    {
            if(!stats.unlockedSellerFactories.contains(t) && t.getConstructionType() != Factory.ConstructionType.BUILDABLE)    {
                if(getMoney().compareTo(t.getPrice()) >= 0)  {
                    stats.unlockedSellerFactories.add(t);
                    stats.updatePurchasableFactoriesList();
                    if(Constants.DEBUG) System.out.println("Added a seller to the list: " + t);
                }
            }
        }
        Core.MainWindow.updateMoney();
    }

    public static void registerFactoryManager(Factory f) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(f.getFactoryType() == Factory.FactoryType.PRIMARY)  {
            PrimaryFactories.add(f);
            Factories.add(f);
            cleanFactories.add((Factory) Class.forName(f.getClass().getName()).newInstance());
            if(Constants.DEBUG) System.out.println("Registered Factory: " + f);
        }
    }

    public static void registerSeller(Factory s) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(s.getFactoryType() == Factory.FactoryType.SELLER) {
            Sellers.add(s);
            Factories.add(s);
            cleanFactories.add((Factory) Class.forName(s.getClass().getName()).newInstance());
            if(Constants.DEBUG) System.out.println("Registered Factory: " + s);
        }
    }

    public static void registerSecondaryFactory(Factory f) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(f.getFactoryType() == Factory.FactoryType.SECONDARY) {
            SecondaryFactories.add(f);
            Factories.add(f);
            cleanFactories.add((Factory) Class.forName(f.getClass().getName()).newInstance());
            if(Constants.DEBUG) System.out.println("Registered Factory: " + f);
        }
    }

    public static void tickPrimaryFactories() {
        for(Factory f : PrimaryFactories) {
            f.produceItems();
        }
    }

    public static void tickSellers()    {
        for(Factory s : Sellers) {
            s.produceItems();
        }
    }

    public static void tickSecondaryFactories() {
        for(Factory f : SecondaryFactories) {
            f.produceItems();
        }
    }
}
