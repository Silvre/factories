package com.Silvre.Factories.resources;

public enum Resource {
    MONEY(-1, "Money"),
    WOOD(0, "Wood"),
    STONE(1, "Stone"),
    COAL(2, "Coal"),
    DIRTYWATER(3, "Dirty Water"),
    DISTILLEDWATER(4, "Distilled Water"),
    CHARCOAL(5, "Charcoal"),
    COPPER(6, "Copper Ore"),
    IRON(7, "Iron Ore"),
    IRONSLUSH(8, "Iron Slush");


    /**
     * Created by Dylan(Silvre) on 9/28/2016.
     *
     * Project: Factories
     */
    private int id;
    private String name;
    Resource(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getID()  { return id;    }
    public String getName() {   return name;    }
}
