package com.Silvre.Factories.io;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

/**
 * Created by Silvre on 4/9/2017.
 * Project: factories
 * If you are reading this - you can read this
 */
public class AES {

    public static String encrypt(String json, String key)   {
        try {
            byte[] bytekey = key.getBytes("UTF-8");
            MessageDigest d = MessageDigest.getInstance("SHA-1");
            bytekey = Arrays.copyOf(d.digest(bytekey), 16);
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(bytekey, "AES"));
            System.out.println(Base64.getEncoder().encodeToString(c.doFinal(json.getBytes())));
            return Base64.getEncoder().encodeToString(c.doFinal(json.getBytes()));
        }
        catch(Exception e)  {
            System.out.println("Encryption Failed.");
        }
        return "Encryption Failed. Rip.";
    }

    public static String decrypt(String message, String key)    {
        try {
            byte[] bytekey = key.getBytes();
            MessageDigest d = MessageDigest.getInstance("SHA-1");
            bytekey = Arrays.copyOf(d.digest(bytekey), 16);
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(bytekey, "AES"));
            System.out.println(new String(c.doFinal(Base64.getDecoder().decode(message))));
            return new String(c.doFinal(Base64.getDecoder().decode(message)));
        } catch (Exception e) {
            System.out.println("Decryption Failed");
        }
        return "Decryption Failed. Rip.";
    }


}
