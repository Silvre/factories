package com.Silvre.Factories.io;

import com.Silvre.Factories.game.Statistics;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;
import com.Silvre.Factories.util.Duplet;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Silvre on 4/8/2017.
 * Project: factories
 * If you are reading this - you can read this
 */
public class PlayerInfo {
    public boolean isFirstTime;
    public int tutorialClicks;
    public Set<String> unlockedPrimary, unlockedSeller, unlockedSecondary;
    public ArrayList<Duplet<Resource, CompactDouble>> resourceList = new ArrayList<>();
    public PlayerInfo(Statistics s) {
        tutorialClicks = s.getTutorialClicks();
        isFirstTime = s.isFirstTime();
        unlockedPrimary = new TreeSet<>();
        s.unlockedPrimaryFactories.forEach(x -> unlockedPrimary.add(Statistics.getFactoryName(x)));
        unlockedSeller = new TreeSet<>();
        s.unlockedSellerFactories.forEach(x -> unlockedSeller.add(Statistics.getFactoryName(x)));
        unlockedSecondary = new TreeSet<>();
        s.unlockedSecondaryFactories.forEach(x -> unlockedSecondary.add(Statistics.getFactoryName(x)));
        resourceList = s.resourceList;
    }
}
