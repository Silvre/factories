package com.Silvre.Factories.interfaces;

import com.Silvre.Factories.game.Constants;
import com.Silvre.Factories.game.Core;
import com.Silvre.Factories.game.ResourceManager;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;
import com.Silvre.Factories.util.RomanNumbers;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 9/29/2016.
 *
 * Project: Factories
 */
public abstract class Factory implements Comparable<Factory> {

    public enum FactoryType {
        PRIMARY,
        SECONDARY,
        SELLER
    }

    public enum ConstructionType    {
        PURCHASABLE, BUILDABLE, BOTH
    }

    private String name = "Error!";
    private CompactDouble baseprice = null;
    private CompactDouble price = null;
    private CompactDouble coefficient = null;
    private ConstructionType ctype = null;
    private FactoryType ftype= null;
    private int numberOwned = 0;
    private HashMap<Resource, CompactDouble> products;
    private HashMap<Resource, CompactDouble> ingredients;
    private int tier = 1;//todo add upgrading tiers

    public Factory(ConstructionType t)   {
        ctype = t;
    }

    public Factory(String s) {
        name = s;
    }

    public CompactDouble getBasePrice() {
        return baseprice;
    }

    public void setBasePrice(CompactDouble d)   {
        if(getConstructionType() != ConstructionType.BUILDABLE) baseprice = d;
    }

    public CompactDouble getPrice() {
        return price;
    }

    public void setPrice(CompactDouble d)   {
        if(getConstructionType() != ConstructionType.BUILDABLE) price = d;
    }

    public CompactDouble getCoefficient()   {
        return coefficient;
    }

    public void setCoefficient(CompactDouble d) {
        if(getConstructionType() != ConstructionType.BUILDABLE) coefficient = d;
    }

    public void setConstructionType(ConstructionType d) {
        ctype = d;
    }

    public void setFactoryType(FactoryType d) {
        ftype = d;
    }

    public ConstructionType getConstructionType()   {
        return ctype;
    }

    public FactoryType getFactoryType()   {
        return ftype;
    }

    public void setProducts(HashMap delta)   {
        products = delta;
    }

    public HashMap<Resource, CompactDouble> getProducts()   {
        return products;
    }

    public void setIngredients(HashMap d)   {
        ingredients = d;
    }

    public HashMap<Resource, CompactDouble> getIngredients()    {
        return ingredients;
    }

    //public HashMap<Resource, CompactDouble> getSummedProducts()   {
    //    return summedProducts;
    //}

    public void produceItems()  {
        if(ingredients != null) {
            for(Resource r : ingredients.keySet())  {
                if(ResourceManager.getAmount(r).compareTo(ingredients.get(r).clone().multiply(numberOwned)) == -1)    {
                    if(Constants.DEBUG) System.out.println(this.toString() + " cannot produce as there is not enough ingredients");
                    return;
                }
            }
            for(Resource r : ingredients.keySet())  {
                CompactDouble num = ingredients.get(r).clone().multiply(numberOwned);
                if(num.getValue() != 0) ResourceManager.subtract(r, num);
            }

        }
        for(Resource r : products.keySet()) {
            CompactDouble num = products.get(r).clone();
            num.multiply(numberOwned);
            if(num.getValue() != 0)  Core.stats.add(r, num);
        }
    }

    /**
     * Use to make a specific algorithm in which the costs(or whatever) increases.
     * Use it to tailor a specific factory.
     */
    public abstract void purchaseAlgorithm();

    public void purchaseItem()  {
        if(getConstructionType() == ConstructionType.PURCHASABLE || getConstructionType() == ConstructionType.BOTH) {
            if(ResourceManager.getMoney().compareTo(getPrice()) >= 0)   {
                numberOwned++;
                ResourceManager.getMoney().subtract(getPrice());
                purchaseAlgorithm();
            }
        }
    }
    //todo add building

    @Override
    public int compareTo(Factory f) {
        return price.compareTo(f.price);
    }

    public int getNumberOwned() {
        return numberOwned;
    }

    @Override
    public String toString() {
        return name + " Mk " + RomanNumbers.toRoman(tier);
    }

    public String getName() {   return name;    }

    public Factory clone() {
        try {
            return getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
