package com.Silvre.Factories.handler;

import com.Silvre.Factories.game.Core;
import com.Silvre.Factories.game.ResourceManager;
import com.Silvre.Factories.game.ShopController;
import com.Silvre.Factories.interfaces.Factory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Created by Dylan(Silvre) on 12/17/2016.
 * <p>
 * Project: factories
 */
public class SecondaryFactoryItemSelectedHandler implements ChangeListener<String> {
    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        ShopController s = Core.ShopControl;
        s.selectedSecondaryFactory = null;
        for(Factory f : ResourceManager.SecondaryFactories) {
            if(f.toString().equals(newValue) && f.getConstructionType() != Factory.ConstructionType.BUILDABLE)  {
                s.selectedSecondaryFactory = f;
            }
        }
        s.updateSecondaryButton();
        s.updateSecondaryDescription();
    }
}
