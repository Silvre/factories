package com.Silvre.Factories.handler;

import com.Silvre.Factories.game.*;
import com.Silvre.Factories.interfaces.Factory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Created by Dylan(Silvre) on 10/22/2016.
 * <p>
 * Class: ${CLASS}
 */
public class PrimaryFactoryItemSelectedHandler implements ChangeListener<String> {
    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        ShopController s = Core.ShopControl;
        s.selectedPrimaryFactory = null;
        for(Factory f : ResourceManager.PrimaryFactories)    {
            if(f.toString().equals(newValue) && f.getConstructionType() != Factory.ConstructionType.BUILDABLE)   {
                s.selectedPrimaryFactory = f;
            }
        }
        s.updatePrimaryButton();
        s.updatePrimaryDescription();
    }
}
