package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 11/13/2016.
 * Project: Factories
 */

public class WoodSeller extends Factory {
    private static HashMap<Resource, CompactDouble> ingredients = new HashMap<>();
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        ingredients.put(Resource.WOOD, new CompactDouble(1,0));
        products.put(Resource.MONEY, new CompactDouble(1, 0));
    }

    public WoodSeller() {
        super("Wood Seller");
        this.setConstructionType(ConstructionType.PURCHASABLE);
        this.setFactoryType(FactoryType.SELLER);
        setPrice(new CompactDouble(2, 1));
        setBasePrice(new CompactDouble(2, 1));
        setCoefficient(new CompactDouble(1.11, 0));
        setIngredients(ingredients);
        setProducts(products);
    }

    @Override
    public void purchaseAlgorithm() {
        setPrice(getPrice().multiply(getCoefficient()));
        setPrice(getPrice().add(new CompactDouble(5)));
    }
}
