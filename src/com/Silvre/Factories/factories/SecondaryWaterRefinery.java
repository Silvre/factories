package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 1/4/2017.
 * Project: factories
 */
public class SecondaryWaterRefinery extends Factory {
    private static HashMap<Resource, CompactDouble> ingredients = new HashMap<>();
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        ingredients.put(Resource.DIRTYWATER, new CompactDouble(2,0));
        products.put(Resource.DISTILLEDWATER, new CompactDouble(2,0));
    }

    public SecondaryWaterRefinery() {
        super("Water Distillation Tower");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.SECONDARY);
        setBasePrice(new CompactDouble(1, 5));
        setPrice(new CompactDouble(1, 5));
        setCoefficient(new CompactDouble(1.05, 0));
        setProducts(products);
        setIngredients(ingredients);
    }
    @Override
    public void purchaseAlgorithm() {
        getPrice().multiply(getCoefficient());
        getPrice().add(new CompactDouble(5, 3));
    }
}
