package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Team Tower Defense on 12/16/2016.
 */
public class PrimaryWaterPump extends Factory {
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        products.put(Resource.DIRTYWATER, new CompactDouble(1));
    }

    public PrimaryWaterPump()    {
        super("Water Pump");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.PRIMARY);
        setBasePrice(new CompactDouble(3, 5));
        setPrice(new CompactDouble(3, 5));
        setCoefficient(new CompactDouble(1.04, 0));
        setProducts(products);

    }
    @Override
    public void purchaseAlgorithm() {
        getPrice().multiply(getCoefficient());
        getPrice().add(new CompactDouble(2, 3));
    }
}
