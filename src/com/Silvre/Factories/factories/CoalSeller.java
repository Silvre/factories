package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 12/14/2016.
 * <p>
 * Project: factories
 */
public class CoalSeller extends Factory {
    private static HashMap<Resource, CompactDouble> ingredients = new HashMap<>();
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        ingredients.put(Resource.COAL, new CompactDouble(1));
        products.put(Resource.MONEY, new CompactDouble(500));
    }


    public CoalSeller() {
        super("Coal Seller");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.SELLER);
        setPrice(new CompactDouble(1.2, 4));
        setBasePrice(new CompactDouble(1.2, 4));
        setCoefficient(new CompactDouble(1.12, 0));
        setIngredients(ingredients);
        setProducts(products);
    }

    @Override
    public void purchaseAlgorithm() {
        getPrice().add(new CompactDouble(1.3, 3));
        getPrice().multiply(getCoefficient());
    }
}
