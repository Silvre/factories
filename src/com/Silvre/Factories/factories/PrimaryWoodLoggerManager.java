package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;
import com.Silvre.Factories.game.*;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 9/30/2016.
 *
 * Project: Factories
 */
public class PrimaryWoodLoggerManager extends Factory {
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        products.put(Resource.WOOD, new CompactDouble(1, 0));
    }

    public PrimaryWoodLoggerManager()  {
        super("Wood Logger");
        this.setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.PRIMARY);
        setPrice(new CompactDouble(2.5, 1));
        setBasePrice(new CompactDouble(2.5, 1));
        setCoefficient(new CompactDouble(1.08, 0));
        setProducts(products);
    }

    @Override
    public void purchaseAlgorithm() {
        setPrice(getPrice().multiply(getCoefficient()));
        setPrice(getPrice().add(new CompactDouble(3)));
    }
}
