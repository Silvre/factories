package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 12/14/2016.
 * <p>
 * Project: factories
 */
public class PrimaryCoalMine extends Factory {

    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        products.put(Resource.COAL, new CompactDouble(1));
    }

    public PrimaryCoalMine()    {
        super("Coal Mine");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.PRIMARY);
        setBasePrice(new CompactDouble(1, 4));
        setPrice(new CompactDouble(1, 4));
        setCoefficient(new CompactDouble(1.08, 0));
        setProducts(products);

    }
    @Override
    public void purchaseAlgorithm() {
        getPrice().multiply(getCoefficient());
        getPrice().add(new CompactDouble(9,2));
    }
}
