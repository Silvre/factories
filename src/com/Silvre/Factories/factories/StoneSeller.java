package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 12/12/2016.
 * <p>
 * Project: factories
 */
public class StoneSeller extends Factory {
    private static HashMap<Resource, CompactDouble> ingredients = new HashMap<>();
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static  {
        ingredients.put(Resource.STONE, new CompactDouble(1));
        products.put(Resource.MONEY, new CompactDouble(10));
    }

    public StoneSeller()    {
        super("Stone Seller");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.SELLER);
        setPrice(new CompactDouble(6, 2));
        setBasePrice(new CompactDouble(6, 2));
        setCoefficient(new CompactDouble(1.13, 0));
        setIngredients(ingredients);
        setProducts(products);

    }

    @Override
    public void purchaseAlgorithm() {
        getPrice().multiply(getCoefficient());
        getPrice().add(new CompactDouble(30));
    }
}
