package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Team Tower Defense on 1/6/2017.
 */
public class SecondaryCharcoalFactory extends Factory {
    private static HashMap<Resource, CompactDouble> ingredients = new HashMap<>();
    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static  {
        ingredients.put(Resource.COAL, new CompactDouble(5));
        ingredients.put(Resource.DISTILLEDWATER, new CompactDouble(1));
        products.put(Resource.CHARCOAL, new CompactDouble(1));
    }

    public SecondaryCharcoalFactory()   {
        super("Charcoal Factory");
        setConstructionType(ConstructionType.PURCHASABLE);
        setFactoryType(FactoryType.SECONDARY);
        setBasePrice(new CompactDouble(1.5, 5));
        setPrice(new CompactDouble(1.5, 5));
        setCoefficient(new CompactDouble(1.07, 0));
        setProducts(products);
        setIngredients(ingredients);
    }
    @Override
    public void purchaseAlgorithm() {

    }
}
