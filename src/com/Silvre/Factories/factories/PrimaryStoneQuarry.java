package com.Silvre.Factories.factories;

import com.Silvre.Factories.interfaces.Factory;
import com.Silvre.Factories.resources.Resource;
import com.Silvre.Factories.util.CompactDouble;

import java.util.HashMap;

/**
 * Created by Dylan(Silvre) on 12/10/2016.
 * <p>
 * Project: factories
 */
public class PrimaryStoneQuarry extends Factory {

    private static HashMap<Resource, CompactDouble> products = new HashMap<>();

    static {
        products.put(Resource.STONE, new CompactDouble(1));
    }

    public PrimaryStoneQuarry() {
        super("Stone Quarry");
        setFactoryType(FactoryType.PRIMARY);
        setConstructionType(ConstructionType.PURCHASABLE);
        setPrice(new CompactDouble(5,2));
        setBasePrice(new CompactDouble(5,2));
        setCoefficient(new CompactDouble(1.1, 0));
        setProducts(products);
    }
    @Override
    public void purchaseAlgorithm() {
        setPrice(getPrice().add(new CompactDouble(20)));
        setPrice(getPrice().multiply(getCoefficient()));

    }
}
